---
version: 7.0.39
authors: MIRS
numbersections: true
---



# Cover Page

- Copyright Statement
- Acknowledgements
- Contact info


# DICOM Conformance Statement

# Table of Contents
<!-- TOC -->

- [Cover Page](#cover-page)
- [DICOM Conformance Statement](#dicom-conformance-statement)
- [Table of Contents](#table-of-contents)
- [Overview](#overview)
    - [Purpose](#purpose)
- [Introduction](#introduction)
    - [Revision History](#revision-history)
    - [Audience](#audience)
    - [Remarks](#remarks)
    - [Document Conventions](#document-conventions)
    - [Terms and definitions](#terms-and-definitions)
    - [Basics of DICOM Communications](#basics-of-dicom-communications)
    - [Abbreviations](#abbreviations)
    - [References](#references)
- [Networking](#networking)
    - [Implementation Model](#implementation-model)
        - [Application Data Flow](#application-data-flow)
        - [Functional Definition of MIRS SCU](#functional-definition-of-mirs-scu)
            - [CT Export](#ct-export)
            - [MR](#mr)
            - [RT Plan Export](#rt-plan-export)
            - [RT Dose](#rt-dose)
            - [RT Structure](#rt-structure)
            - [RT Image](#rt-image)
    - [AE Specifications](#ae-specifications)
        - [MIRS SCU](#mirs-scu)
        - [SOP Classes](#sop-classes)
        - [Association Policies for MIRS SCU](#association-policies-for-mirs-scu)
            - [General](#general)
            - [Number of Associations](#number-of-associations)
            - [Asynchronous Nature](#asynchronous-nature)
            - [Implementation Identifying Information](#implementation-identifying-information)
        - [Association Initiation Policy for MIRS SCU](#association-initiation-policy-for-mirs-scu)
            - [Description and Sequencing of activities](#description-and-sequencing-of-activities)
            - [Proposed Presentation Contexts](#proposed-presentation-contexts)
            - [SOP Specific Conformance for SOP Classes](#sop-specific-conformance-for-sop-classes)
                - [SOP Specific Conformance for CT Image Storage SOP Class with MIRS SCU](#sop-specific-conformance-for-ct-image-storage-sop-class-with-mirs-scu)
                - [SOP Specific Conformance for RT Plan Storage SOP Class with MIRS SCU](#sop-specific-conformance-for-rt-plan-storage-sop-class-with-mirs-scu)
                - [SOP Specific Conformance for RT Dose Storage SOP Class with MIRS SCU](#sop-specific-conformance-for-rt-dose-storage-sop-class-with-mirs-scu)
                - [SOP Specific Conformance for RT Structure Set Storage SOP Class with MIRS SCU](#sop-specific-conformance-for-rt-structure-set-storage-sop-class-with-mirs-scu)
                - [SOP Specific Conformance for RT Image Storage SOP Class with MIRS SCU](#sop-specific-conformance-for-rt-image-storage-sop-class-with-mirs-scu)
        - [MIRS SCP](#mirs-scp)
    - [Network Interfaces](#network-interfaces)
        - [Physical Network Interface](#physical-network-interface)
        - [Additional Protocols](#additional-protocols)
        - [IPv4 and IPv6 Support](#ipv4-and-ipv6-support)
    - [Configuration](#configuration)
    - [AE Title/Presentation Mapping](#ae-titlepresentation-mapping)
    - [Local AE Titles](#local-ae-titles)
    - [Remote AE Title/Presentation Address Mapping](#remote-ae-titlepresentation-address-mapping)
        - [Remote SCP](#remote-scp)
        - [Parameters](#parameters)
- [Support of Character Sets](#support-of-character-sets)
- [Security](#security)
- [Annexes](#annexes)
    - [Appendix A : Common Modules Export](#appendix-a--common-modules-export)
        - [C.7.1 Patient Module Attributes](#c71-patient-module-attributes)
        - [C.7.2.1. General Study Module Attributes](#c721-general-study-module-attributes)
        - [C.7.2.1. Patient Study Module Attributes](#c721-patient-study-module-attributes)
        - [C.7.3.1 General Series Module](#c731-general-series-module)
        - [C.7.4.1 Frame of Reference Module](#c741-frame-of-reference-module)
        - [C.7.5.1 General Equipment Module](#c751-general-equipment-module)
        - [C.7.6.1 General Image Module](#c761-general-image-module)
        - [C.7.6.2 Image Plane Module](#c762-image-plane-module)
        - [C.7.6.3 Image Pixel Module](#c763-image-pixel-module)
        - [C.12.1 SOP Common Module](#c121-sop-common-module)
        - [C.12.4 General Reference](#c124-general-reference)
        - [C.11.2 VOI LUT Module](#c112-voi-lut-module)
    - [Appendix B : CT Image Export](#appendix-b--ct-image-export)
        - [C.8.2.1 CT Image Module](#c821-ct-image-module)
    - [Appendix D : RT Plan Export](#appendix-d--rt-plan-export)
    - [Appendix E : RT Dose Export](#appendix-e--rt-dose-export)
    - [Appendix F : RT Structure Set Export](#appendix-f--rt-structure-set-export)
    - [Appendix G : RT Image Export](#appendix-g--rt-image-export)
    - [Appendix H: MR IMage Export](#appendix-h-mr-image-export)

<!-- /TOC -->



# Overview

## Purpose

This is the DICOM conformance statement for MIRS, the Modular Integrated Radiotherapy System for Radiotherapy and RadioSurgery. 

MIRS provides three-dimensional planning for several radiation treatment modalities such as Conformal Radiation Therapy (3D-CRT), Electron Beam Radiation Therapy (EBRT), Radio-surgery with Linear Accelerators and Intensity Modulated Radiation Therapy (IMRT). 

As of release 7.0.31, MIRS supports the network export of CT Images, RT Structure Sets, RT Plans, RT Dose and digitally reconstructed radiographs (DDRs) as RT Image objects. All this data is exported as DICOM objects to a  Provider of Service (SCP), or to files, selected by the user at export time.

A summary of the network services provided by MIRS can be found in the following tables:

**Table 1.1: Network Services**

| SOP Class             | User of Service 􏰀SCU􏰁 | Provider of Service 􏰀SCP􏰁 |
|-----------------------|-----------------------|---------------------------|
| **Transfer**          |                       |                           |
| CT Image Storage      | Yes                   | No                        |
| MR Image Storage      | Yes                   | No                        |
| STRUCTURE SET Storage | Yes                   | No                        |
| RT PLAN Storage       | Yes                   | No                        |
| RT Dose Storage       | Yes                   | No                        |
| RT Image Storage      | Yes                   | No                        |

**UID Values**

| UID Value                     | UID Name                 | Category |
|-------------------------------|--------------------------|----------|
| 1.2.840.10008.1.1             | Verification             | Transfer |
| 1.2.840.10008.5.1.4.1.1.2     | CT Image Storage         | Transfer |
| 1.2.840.10008.5.1.4.1.1.4     | MR Image Storage         | Transfer |
| 1.2.840.10008.5.1.4.1.1.481.3 | RT Structure Set Storage | Transfer |
| 1.2.840.10008.5.1.4.1.1.481.5 | RT Plan Storage          | Transfer |
| 1.2.840.10008.5.1.4.1.1.481.2 | RT Dose Storage          | Transfer |
| 1.2.840.10008.5.1.4.1.1.481.1 | RT Image Storage         | Transfer |


# Introduction

## Revision History

**v 7.0.31**, *8/28/2018*: Initial draft release.


## Audience

This document is aimed for the users that need to understand the functionality of the DICOM export/import capabilities of MIRS. This document will be most useful for users that are responsible for setting up, managing and/or maintaining the DICOM network in their respective healthcare facilities. Any reader will be able to understand the implementation of the DICOM protocol by MIRS from this document. For further reference, the DICOM Standard is documented [here](http://dicom.nema.org/medical/dicom/current/output/html/part01.html).

## Remarks

The MIRS application is a three-dimensional treatment planning system for different radiation modalities. MIRS 
can import different images from files to its patient database. With the full visualization of the data, the user can design plans for 3D, IMRT, VMAT, (**NMED** *Aclarar los distintos tipos de planificaciones que MIRS brinda*), using state of the art dose calculation methods. Advanced optimization techniques are applied to enhance the quality of the treatments. These plans are can be exported to files, or stored in DICOM format to any DICOM compliant treatment management system.

## Document Conventions

(**NMED** *Al describir la funcionalidad, se puede aclarar que las opciones del usuario están en tal o cual solapa del menú. Hay que ver si vale la pena aclararlo o no*) 
Describe the menus and command calls if necessary. 

## Terms and definitions

Some of the terms and definitions from [DICOM Conformance Statement PS3.2](http://dicom.nema.org/medical/dicom/current/output/html/part02.html#sect_A.3.4)


**Abstract Syntax**:
The information agreed to be exchanged between applications, generally equivalent to a Service/Object Pair (SOP) Class. Examples: Verification SOP Class, Modality Worklist Information Model Find SOP Class, Computed Radiography Image Storage SOP Class.

**Application Entity (AE)**: An end point of a DICOM information exchange, including the DICOM network or media interface software; i.e., the software that sends or receives DICOM information objects or messages. A single device may have multiple Application Entities.

**Application Entity Title (AET)**: The externally known name of an Application Entity, used to identify a DICOM application to other DICOM applications on the network.

**Association**: A network communication channel set up between Application Entities.

**Attribute**:  A unit of information in an object definition; a data element identified by a tag. The information may be a complex data structure (Sequence), itself composed of lower level data elements. Examples: Patient ID (0010,0020), Accession Number (0008,0050), Photometric Interpretation (0028,0004), Procedure Code Sequence (0008,1032).

**Information Object Definition (IOD)**: The specified set of Attributes that comprise a type of data object; does not represent a specific instance of the data object, but rather a class of similar data objects that have the same properties. The Attributes may be specified as Mandatory (Type 1), Required but possibly unknown (Type 2), or Optional (Type 3), and there may be conditions associated with the use of an Attribute (Types 1C and 2C). Examples: MR Image IOD, CT Image IOD, Print Job IOD.

**Module**: A set of Attributes within an Information Object Definition that are logically related to each other. Example: Patient Module includes Patient Name, Patient ID, Patient Birth Date, and Patient Sex.

**Negotiation**: First phase of Association establishment that allows Application Entities to agree on the types of data to be exchanged and how that data will be encoded.

**Presentation Context**: The set of DICOM network services used over an Association, as negotiated between Application Entities; includes Abstract Syntaxes and Transfer Syntaxes.

**Protocol Data Unit (PDU)**: A packet (piece) of a DICOM message sent across the network. Devices must specify the maximum size packet they can receive for DICOM messages.

**Service Class Provider (SCP)**:
Role of an Application Entity that provides a DICOM network service; typically, a server that performs operations requested by another Application Entity (Service Class User). Examples: Picture Archiving and Communication System (image storage SCP, and image query/retrieve SCP), Radiology Information System (modality worklist SCP).

**Service Class User (SCU)**:
Role of an Application Entity that uses a DICOM network service; typically, a client. Examples: imaging modality (image storage SCU, and modality worklist SCU), imaging workstation (image query/retrieve SCU)

**Service/Object Pair Class (SOP Class)**:
The specification of the network or media transfer (service) of a particular type of data (object); the fundamental unit of DICOM interoperability specification. Examples: Ultrasound Image Storage Service, Basic Grayscale Print Management.

**Service/Object Pair Instance (SOP Instance)**:
An information object; a specific occurrence of information exchanged in a SOP Class. Examples: a specific x-ray image.

**Tag**: A 32-bit identifier for a data element, represented as a pair of four digit hexadecimal numbers, the “group” and the “element”. If the “group” number is odd, the tag is for a private (manufacturer-specific) data element. Examples: (0010,0020) [Patient ID], (07FE,0010) [Pixel Data], (0019,0210) [private data element]

**Transfer Syntax**:
The encoding used for exchange of DICOM information objects and messages. Examples: JPEG compressed (images), little endian explicit value representation.

**Unique Identifier (UID)**:
A globally unique “dotted decimal” string that identifies a specific object or a class of objects; an ISO-8824 Object Identifier. Examples: Study Instance UID, SOP Class UID, SOP Instance UID.

**Value Representation (VR)**:
The format type of an individual DICOM data element, such as text, an integer, a person’s name, or a code. DICOM information objects can be transmitted with either explicit identification of the type of each data element (Explicit VR), or without explicit identification (Implicit VR); with Implicit VR, the receiving application must use a DICOM data dictionary to look up the format of each data element.

## Basics of DICOM Communications

Two Application Entities (devices) that want to communicate with each other over a network using DICOM protocol must first agree on several things during an initial network "handshake". This communication is called Association, and is requested by one of those two devices. In the Association request, the requesting device ask for  specific services, information, and encoding  that are supported by the other device (Negotiation).

DICOM Standard defines network services and types of information objects, each of which is called an Abstract Syntax for the Negotiation. DICOM also specifies a variety of methods for encoding data, denoted Transfer Syntaxes. The Negotiation allows the initiating Application Entity to propose combinations of Abstract Syntax and Transfer Syntax to be used on the Association; these combinations are called Presentation Contexts. The receiving Application Entity accepts the Presentation Contexts it supports.

The Association Negotiation  allows the devices to agree on Roles - which one is the Service Class User (SCU - client) and which is the Service Class Provider (SCP - server). This agreement is determined for each Presentation Context. The Association Negotiation finally establishes  the exchange of maximum network packet (PDU) size, security information, and other network service options (called Extended Negotiation information).

Once the Application Entities had negotiated the Association parameters, they start to exchange data. Common data exchanges include queries for worklists and lists of stored images, transfer of image objects and analyses (structured reports), and sending images to film printers. Each exchangeable unit of data is formatted by the sender in accordance with the appropriate Information Object Definition, and sent using the negotiated Transfer Syntax. There is a Default Transfer Syntax that all systems must accept, but it may not be the most efficient for some use cases. Each transfer is explicitly acknowledged by the receiver with a Response Status indicating success, failure, or that query or retrieve operations are still in process.

Finally, the two Application Entities close the communication channel, when one of the request the release of the Association, and the other one acknowledges this request.

## Abbreviations

**3D**: 3 Dimension

**AE**: Application Entity

**CT**: Computed Tomography

**DICOM**: Digital Imaging and Communication

**DRR**: Digital Reconstructed Radiographs

**ID**: Identifier

**IMRT**: Intensity Modulated Radiation Therapy

**MLC**: Multileaf Collimator

**MR**: Magnetic Resonance

**NEMA**: National Electrical Manufacturers’ Association

**ROI**: Region of Interest

**RT**: Radiotherapy Treatment

**SCP**: Service Class Provider

**SCU**:Service Class User

**SOP**: Standard Operating Procedure

**UID**: Unique Identifier

**VM**: Value Multiplicity

**VMAT**: Volumetric Modulated Arc Therapy

**VR**: Value Representation

## References

 DICOM documents referred along the present guide are:

* [PS3.1: Introduction and Overview](http://dicom.nema.org/medical/dicom/current/output/html/part01.html)

* [PS3.2: Conformance](http://dicom.nema.org/medical/dicom/current/output/html/part02.html#PS3.2)

* [PS3.3: Information Object Definitions](http://dicom.nema.org/medical/dicom/current/output/html/part03.html#PS3.3)

* [PS3.4: Service Class Specifications](http://dicom.nema.org/medical/dicom/current/output/html/part04.html#PS3.4)

* [PS3.5: Data Structures and Encoding](http://dicom.nema.org/medical/dicom/current/output/html/part05.html#PS3.5)

* [PS3.7: Message Exchange](http://dicom.nema.org/medical/dicom/current/output/html/part07.html#PS3.7)

* [PS3.8: Network Communication Support for Message Exchange](http://dicom.nema.org/medical/dicom/current/output/html/part08.html#PS3.8)

Besides, the user can check [MIRS User's Manual]() for further guidance.


# Networking

## Implementation Model

### Application Data Flow 



### Functional Definition of MIRS SCU

MIRS users can configure different SCPs as targets for DICOM export. Each target is defined with the usual AE Title, IP address and port number. Then, the user selects the specific DICOM server from a customizable list of those target Application Entities, and chooses which DICOM objects (CT, MR, RT Plan, RT Dose, RT Structure and/or RT Image) will be stored in that target from a menu. The MIRS application requests the Association to the server, and sends the selected DICOM objects to that server once the Association is accepted. MIRS closes the Association once the transfer is completed.

#### CT Export

MIRS is able to export CT objects within a particular study. The user can choose to export all CT images  in native (512x512) or high resolution (1024x1024) pixels, select a signed Pixel Representation, and allow to export the images with rectangular pixels. Also, the user can select to export a specific set of slices from the CT image. By default, MIRS exports all CT slices in 512x512 pixels of square size, in unsigned pixel representation. 

#### MR

(**NMED**: *No hemos hecho nada en este sentido, quizás lo mejor sea aclarar en este punto que MR no está validado todavía*)

#### RT Plan Export

MIRS exports the Plan as a DICOM RTPlan object, to the user-selected SCP Application Entity selected by the user as target. Course ID can be selected at export time.

#### RT Dose

MIRS exports the Dose as a DICOM RTDose object as a Full Anatomy Dose matrix, or selected planes over the principal axis. Oblique planes can not be exported.

#### RT Structure

MIRS exports the structures as DICOM RTStructure object. It can export any of the selected structures by the user.

#### RT Image

MIRS can export any of the DRR images as a DICOM RTImage object. The user can select the size, Window Width and Level. The Beam Collimation  overlay can be selected to be included in the exported DRR.

*a more detailed specification of each Application Entity, listing the SOP Classes supported and outlining the policies with which it initiates or accepts associations;*

*for each Application Entity and Real-World Activity combination, a description of proposed (for Association Initiation) and acceptable (for Association Acceptance) Presentation Contexts;*


## AE Specifications

### MIRS SCU

### SOP Classes

| SOP Class Name            | SOP Class UID                 | SCU | SCP |
|---------------------------|-------------------------------|-----|-----|
| Verification              | 1.2.840.10008.1.1             | Yes | No  |
| CT Image Storage          | 1.2.840.10008.5.1.4.1.1.2     | Yes | No  |
| MR Image Storage          | 1.2.840.10008.5.1.4.1.1.4     | Yes | No  |
| RT Plan Storage           | 1.2.840.10008.5.1.4.1.1.481.5 | Yes | No  |
| RT Dose Storage           | 1.2.840.10008.5.1.4.1.1.481.2 | Yes | No  |
| RT Structure Set Storage  | 1.2.840.10008.5.1.4.1.1.481.3 | Yes | No  |
| RT Image Storage          | 1.2.840.10008.5.1.4.1.1.481.1 | Yes | No  |


MIRS provides an 

### Association Policies for MIRS SCU

#### General

The 􏰀PDU􏰁 size proposed in any association request is 4096 bytes, and it is not  configurable by the user.

#### Number of Associations
Table 3-4: Number of Associations as an Association Initiator for CMS_SCU

|Maximum number of simultaneous associations             | 1 | 
|------------------------------------------------------|---|

#### Asynchronous Nature
This is not supported.

####  Implementation Identifying Information

(**NMED**: *Completar la siguiente tabla, el UID es dependiente de la licencia? Habría que aclararlo*)
The implementation information for the Storage Application Entity is:

| Implementation Class UID	 | 1.3.6.1.4.1.32947 xxx | 
|------------------------------------------------------|---|
| Implementation Version Name	 | MIRS_xxx| 



### Association Initiation Policy for MIRS SCU

The MIRS application requests the Association to the server, and sends the selected DICOM objects to that server once the Association is accepted. MIRS closes the Association once the transfer is completed. The default name for the MIRS AE title is (**NMED**: *Completar*).

#### Description and Sequencing of activities

#### Proposed Presentation Contexts

| Presentation Context  |           Table                |                                 |                   |      |                      |
|----------------------------|---------------------------|---------------------------------|-------------------|------|----------------------|
| **Abstract Syntax**        |                               | **Transfer Syntax**             |            | **Role** | **Extended Negotiation** |
| **Name**                   | **UID**                       | **Name**                        | **UID**           |      |                      |
| CT Image Storage           | 1.2.840.10008.5.1.4.1.1.2     | DICOM Implicit VR Little Endian | 1.2.840.10008.1.2 | SCU  | None                 |
| MR Image Storage           | 1.2.840.10008.5.1.4.1.1.4     | DICOM Implicit VR Little Endian | 1.2.840.10008.1.2 | SCU  | None                 |
| RT Structure Set Storage   | 1.2.840.10008.5.1.4.1.1.481.3 | DICOM Implicit VR Little Endian | 1.2.840.10008.1.2 | SCU  | None                 |
| RT Dose Storage            | 1.2.840.10008.5.1.4.1.1.481.2 | DICOM Implicit VR Little Endian | 1.2.840.10008.1.2 | SCU  | None                 |
| RT Plan Storage            | 1.2.840.10008.5.1.4.1.1.481.5 | DICOM Implicit VR Little Endian | 1.2.840.10008.1.2 | SCU  | None                 |
| RT Image Storage           | 1.2.840.10008.5.1.4.1.1.481.1 | DICOM Implicit VR Little Endian | 1.2.840.10008.1.2 | SCU  | None                 |


#### SOP Specific Conformance for SOP Classes

DICOM Tag (0010,0010) for Patient Name and DICOM Tag (0010,0020) for Patient ID are the same for all the DICOM RT objects exported at a given time. DICOM Tag (0010,0030) for Patient's Birth Date and (0010,0040) are set at Patient creation within MIRS application, and inherited in each export.

Study instance UID (0020,000D) is persistent across different exports at different times. 

DICOM Tag (0008, 0070) for Manufacturer is **NUCLEMED SA** for all exported objects, as well as DICOM Tag (0008, 1090) Manufacturer's Model Name, which default to 'MIRS'.

##### SOP Specific Conformance for CT Image Storage SOP Class with MIRS SCU

For any given set of CT Slices, MIRS will export 256 slices, and it is not a "pass-through" transformation of the object.

##### SOP Specific Conformance for RT Plan Storage SOP Class with MIRS SCU

The export of each RT Plan generates a unique instance UID, therefore RT Plan object UIDs are not currently persistent.

##### SOP Specific Conformance for RT Dose Storage SOP Class with MIRS SCU

The export of a RT Dose object is always a fixed 128x128x128 array of dose values.
The export of each RT Dose generates a unique instance UID, therefore RT Dose object UIDs are not currently persistent.

##### SOP Specific Conformance for RT Structure Set Storage SOP Class with MIRS SCU

The export of each RT Structure Set generates a unique instance UID, therefore RT Structure object UIDs are not currently persistent.

##### SOP Specific Conformance for RT Image Storage SOP Class with MIRS SCU

DDR are exported as RT Image DICOM Object of 1024x1024 pixels, 
independently of the user-selected low, medium or high resolution used to compute each DRR, and irrespective to the Voltage chosen. The export of each RT Image generates a unique instance UID, therefore RT Image object UIDs are not currently persistent.

### MIRS SCP

At present time, MIRS does not provide SCP capabilities.


## Network Interfaces

### Physical Network Interface
The application is indifferent to the physical medium over which TCP/IP executes, which is dependent on the underlying operating system and hardware

### Additional Protocols
When host names rather than IP addresses are used in the configuration properties to specify presentation addresses for remote AEs, the application is dependent on the name resolution mechanism of the underlying operating system.

### IPv4 and IPv6 Support
This product supports both IPv4 and IPv6. It does not utilize any of the optional configuration identification or security features of IPv6.

It inherits the TCP/IP capabilities of the Windows operating system that it is running on.

## Configuration

## AE Title/Presentation Mapping

## Local AE Titles

## Remote AE Title/Presentation Address Mapping

### Remote SCP


### Parameters


|  General Parameters  | Configurable  (Yes/No)   |  Default  Value  |
|---------------------------------------------------------------------------------|---|---|
| Time-out waiting for acceptance or rejection Response to an Association Open Request. (Application Level timeout) |    |    |
| General DIMSE level time-out values |    |    |
| Time-out waiting for response to TCP/IP connect request. (Low-level timeout) |    |    |
| Time-out waiting for acceptance of a TCP/IP message over the network. (Low-level timeout) |    |    |
| Time-out for waiting for data between TCP/IP packets. (Low-level timeout) |    |    |
| Any changes to default TCP/IP settings, such as configurable stack parameters. |    |    |
| Definition of arbitrarily chosen origins |    |    |
| Definition of constant values used in Dose Related Distance Measurements |    |    |
| Other configurable parameters |    |    |
| **AE Specific Parameters** |    |    |
| Size constraint in maximum object size (see note) |    |    |
| Maximum PDU size the AE can receive |    |    |
| Maximum PDU size the AE can send |    |    |
| AE specific DIMSE level time-out values |    |    |
| Number of simultaneous Associations by Service and/or SOP Class |    |    |
| <SOP Class support> (e.g., Multi-frame vs. single frame vs. SC support), when configurable |    |    |
| <Transfer Syntax support>, e.g., JPEG, Explicit VR, when configurable |    |    |
| Other parameters that are configurable |    |    |


# Support of Character Sets

# Security

# Annexes 


