import sys
import os


def seq(s):
    k = s.split('(')[0].count(' ')/3
    # print(int(k))
    return int(k) * '>'


def splitdcm(line):
    if(',' in line):
        # Process tag
        s = line.split(',')
        group = s[0]
        # Process sequence
        k = seq(group) + group.split('(')[1]

        element = s[1].split(')')[0]
        val = s[1].split(')')[1] 
        d = val.split(':')[0]
        d = d[:-2]  # Remove two characters
        description = d.strip()

        md = '| ' + k + ' | ' + element + ' | ' + description + ' | '
        # print(md)
        # if('Private' not in md):

        return k,element,description

    return 0,0,0
            
def read_appendix(fq):

    appendix = {}
    index = 0
    for line in fq:
        #print(line)
        s = line.split(" ")
        #print(s[0])
        # This is the title of the module
        if(s[0]=="###"):
            title = line.split("### ")[-1].strip()
            # print(title)
        if(s[0]=="|"):
            t = line.split("|")[1:-1]
            if(not 'Group' in t[0]):
                # print(t)
                # append list to dict
                t.append(title)
                # print(t)
                appendix[index] = t
                index = index+1
    
    return appendix
        

def compare(dicom,appendix):

    for i,dcm in dicom.items():
        # print(dcm)
        dcm_tag = dcm[0]
        dcm_elem = dcm[1]
        dcm_desc = dcm[2]
        for j,app in appendix.items():
            # print(app)
            app_tag = app[0].strip()
            app_elem = app[1].strip()
            app_desc = app[2].strip()
            app_titl = app[3].strip()
            found = False
            if(app_tag == dcm_tag and app_elem == dcm_elem):
                found = True
                dcm[3] = dcm[3]+1
                break
        if(found):        
            print("Module: "+app_titl+" tag:("+dcm_tag+","+dcm_elem+") ")
        # else:
        #     print("Tag not found: ("+app_tag+","+app_elem+"): ",app_desc)

        





def main():
    filename = sys.argv[1]
    fileappendix = ['appendixA.md','appendixB.md','appendixD.md','appendixE.md','appendixF.md','appendixG.md']
    # print(filename)

    dicom = {}

    # Open dicom and read tags
    fp = open(filename,'r')    
    index = 0
    for line in fp:
        tag, element, description = splitdcm(line)
        if(tag!=0):
            dicom[index] = [tag.strip(), element.strip(), description.strip(),0]
            print(dicom[index])
            index = index + 1

    # Open Appendix and read tables
    #       
    for annex in fileappendix:
        appendix = {}
        print("Checking Appendix: "+annex)
        fq = open("docs/"+annex,'r')        
        appendix = read_appendix(fq)      
        # print(appendix)

        # Check if dicom is in appendix
        compare(dicom,appendix)

    for i,val in dicom.items():
        if(val[3]==0):
            print("Tag not found: ", val)
        
    


if __name__ == '__main__':
    main()