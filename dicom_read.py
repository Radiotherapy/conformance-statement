import sys
import os
import pydicom 
# import matplotlib.pyplot as plt
import numpy as np

# ext = '.dcm'

# filename = "../VALIDACION_RTPlan/Test 0/mirs beta 33/RTDOSE_Matrix_PatID_INV0012_CaseID _01_20180828215846"
# filename = "../VALIDACION_RTPlan/Test 0/mirs beta 38/RTDOSE_Matrix_PatID_INV0012_CaseID _01_20180907124951"



def main():
	filename = sys.argv[1]
	print("Reading: ",filename)
	dcm_st = pydicom.read_file(filename, force=True)
	print(dcm_st)

if __name__ == '__main__':
	main()



# if not hasattr(dcm_st.file_meta, 'TransferSyntaxUID'):
# 	dcm_st.file_meta.TransferSyntaxUID = pydicom.uid.ImplicitVRLittleEndian

# for t in dcm_st.iterall():
	# print(t.tag,t.description())
	# print(dcm_st.data_element("ImageType"))

###################
#### Para DOSIS
###################

# data_array = dcm_st.pixel_array*dcm_st.DoseGridScaling 

# print('Minimo: ',data_array.min())
# print('Maximo: ',data_array.max())
# print('Maximo: ',data_array.shape)

# Slices = len(dcm_st.GridFrameOffsetVector)
# print(Slices)
# data_array.reshape(Slices, dcm_st.Rows,dcm_st.Columns)

# step_x = dcm_st.PixelSpacing[1]
# step_y = dcm_st.PixelSpacing[0]
# axis_x = np.arange(0, dcm_st.Columns*step_x, step_x)
# axis_y = np.arange(0, dcm_st.Rows*step_y, step_y) 

# print(data_array.shape)

# plt.figure()
# plt.imshow(data_array[Slices//2,:,:], aspect = 'auto', extent = [axis_x[0], axis_x[-1], axis_y[-1], axis_y[0]] ,cmap='gray')
# plt.show()

###################
#### Para CTs
###################

# data_array = dcm_st.pixel_array*dcm_st.RescaleSlope + dcm_st.RescaleIntercept

# print('Minimo: ',data_array.min())
# print('Maximo: ',data_array.max())
# print('Maximo: ',data_array.shape)



# step_x = dcm_st.PixelSpacing[0]
# step_y = dcm_st.PixelSpacing[1]
# axis_x = np.arange(0, dcm_st.Columns*step_x, step_x)
# axis_y = np.arange(0, dcm_st.Rows*step_y, step_y) 

# print(data_array.shape)

# plt.figure()
# plt.imshow(data_array, aspect = 'equal', extent = [axis_x[0], axis_x[-1], axis_y[-1], axis_y[0]] ,cmap='gray')
# plt.show()

# filename = "../v7.0.00 Beta 38/prueba sistemas coordenadas 2/dicom/RTPLAN_PatID_INV0020_CaseID _001_Plan_Plan 1_20180907151625"
# filename = "../v7.0.00 Beta 38/prueba sistemas coordenadas 2/iec/RTPLAN_PatID_INV0020_CaseID _001_Plan_Plan 1_20180907151505"
# filename = "../v7.0.00 Beta 38/prueba sistemas coordenadas 2/system/RTPLAN_PatID_INV0020_CaseID _001_Plan_Plan 1_20180907151328"

# filename = "../VALIDACION_RTPlan/Test 0/mirs beta 33/IMAGE_STUDY_ct cirs anon_Slice1_PatID_INV0012_CaseID _01_20180828215846"
# filename = "../CTs/ct cirs anom/CT1_image00018"