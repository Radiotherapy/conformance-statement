import sys
import os


def seq(s):
    k = s.split('(')[0].count(' ')/3
    # print(int(k))
    return int(k) * '>'


def splitdcm(line):
    if(',' in line):
        # Process tag
        s = line.split(',')
        group = s[0]
        # Process sequence
        k = seq(group) + group.split('(')[1]

        element = s[1].split(')')[0]
        val = s[1].split(')')[1] 
        d = val.split(':')[0]
        d = d[:-2]  # Remove two characters
        description = d.strip()

        md = '| ' + k + ' | ' + element + ' | ' + description + ' | '
        if('Private' not in md):
            print(md)


def main():
    filename = sys.argv[1]
    # print(filename)

    fp = open(filename,'r')
    for line in fp:
        splitdcm(line)


if __name__ == '__main__':
    main()