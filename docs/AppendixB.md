## Appendix B : CT Image Export

### CT Common Modules Export


C.7.1 Patient Module Attributes 

C.7.2.1. General Study Module Attributes 

C.7.3.1 General Series Module 

C.7.4.1 Frame of Reference Module 

C.7.5.1 General Equipment Module 

C.7.6.1 General Image Module 

C.7.6.2 Image Plane Module

C.7.6.3 Image Pixel Module 

C.11.2 VOI LUT Module 

C.12.1 SOP Common Module 

### C.8.2.1 CT Image Module
| Group |  Element | Description | 
|-------|----------|-------------|
| 0008 |  0008 | Image Type | 
| 0028 |  0002 | Samples per Pixel | 
| 0028 |  0004 | Photometric Interpretation | 
| 0028 |  0100 | Bits Allocated | 
| 0028 |  0101 | Bits Stored | 
| 0028 |  0102 | High Bit |  
| 0028 |  1052 | Rescale Intercept | 
| 0028 |  1053 | Rescale Slope | 
| 0018 |  1100 | Reconstruction Diameter | 
| 0018 |  1111 | Distance Source to Patient | 
| 0018 |  1120 | Gantry/Detector Tilt | 
| 0018 |  1130 | Table Height | 
