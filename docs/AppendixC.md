## Appendix C : MR Image Export

### MR Common Modules Export

See Appendix A for details.

C.7.1 Patient Module Attributes 

C.7.2.1. General Study Module Attributes 

C.7.3.1 General Series Module 

C.7.4.1 Frame of Reference Module 

C.7.5.1 General Equipment Module 

C.7.6.1 General Image Module 

C.7.6.2 Image Plane Module (**BRCheck** CT only) 

C.7.6.3 Image Pixel Module 

C.11.2 VOI LUT Module 

C.12.1 SOP Common Module 


### C.8.3.1 MR Image Module
| Group |  Element | Description |    
|-------|----------|-------------|
| 0008  |  0008    | Image Type  | 
| 0008 |  0008 | Image Type | 
| 0028 |  0002 | Samples per Pixel | 
| 0028 |  0004 | Photometric Interpretation | 
| 0028 |  0100 | Bits Allocated | 
| 0028 |  0101 | Bits Stored | 
| 0028 |  0102 | High Bit |  
| 0018 |  0020 | Scanning Sequence | 
| 0018 |  0021 | Sequence Variant | 
| 0018 |  0022 | Scan Options | 