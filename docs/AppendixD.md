## Appendix D : RT Plan Export


### Common Modules Export

C.7.1 Patient Module Attributes 

C.7.2.1. General Study Module Attributes 

C.7.3.1 General Series Module 

C.7.5.1 General Equipment Module 

C.7.6.2 Image Plane Module (**BRCheck** CT only) 

C.12.1 SOP Common Module 

### C.8.8.9 RT General Plan Module 
| Group |  Element | Description |    
|-------|----------|-------------|
| 300a |  0002 | RT Plan Label | 
| 300a |  0003 | RT Plan Name | 
| 300a |  0004 | RT Plan Description | 
| 300a |  0006 | RT Plan Date | 
| 300a |  0007 | RT Plan Time | 
| 300a |  000c | RT Plan Geometry | 
| 300c |  0060 | Referenced Structure Set Sequence    | 
| >0008 |  1150 | Referenced SOP Class UID | 
| >0008 |  1155 | Referenced SOP Instance UID | 

### C.8.8.9 RT Prescription Module
| Group |  Element | Description |    
|-------|----------|-------------|
| 300a  |  0010 | Dose Reference Sequence    | 
| >300a |  0012 | Dose Reference Number | 
| >300a |  0014 | Dose Reference Structure Type | 
| >300a |  0016 | Dose Reference Description | 
| >300a |  0020 | Dose Reference Type | 
| >300a |  0026 | Target Prescription Dose | 

### C.8.8.11 Tolerance Table Module
| Group |  Element | Description |    
|-------|----------|-------------|
| 300a  |  0040 | Tolerance Table Sequence    | 
| >300a |  0042 | Tolerance Table Number | 
| >300a |  0043 | Tolerance Table Label | 

### C.8.8.12 RT Patient Setup Module
| Group |  Element | Description |    
|-------|----------|-------------|
| 300a  |  0180 | Patient Setup Sequence    |  
| >0018 |  5100 | Patient Position | 
| >300a |  0182 | Patient Setup Number | 

### C.8.8.13 RT Fraction Scheme Module
| Group |  Element | Description |    
|-------|----------|-------------|
| 300a  |  0070 | Fraction Group Sequence    | 
| >300a |  0071 | Fraction Group Number | 
| >300a |  0078 | Number of Fractions Planned | 
| >300a |  0080 | Number of Beams | 
| >300a |  00a0 | Number of Brachy Application Setups: Exports 0| 
| >300c |  0004 | Referenced Beam Sequence | 
| >>300a |  0082 | Beam Dose Specification Point | 
| >>300a |  0084 | Beam Dose | 
| >>300a |  0086 | Beam Meterset | 
| >>300c |  0006 | Referenced Beam Number | 
| >300c |  0050 | Referenced Dose Reference Sequence    | 
| >>300c |  0051 | Referenced Dose Reference Number | 


### C.8.8.13 RT Fraction Scheme Module
| Group |  Element | Description |    
|-------|----------|-------------|
| 300a  |  00b0 | Beam Sequence  | 
| >300a |  00b2 | Treatment Machine Name: Exports Target Machine Name| 
| >300a |  00b3 | Primary Dosimeter Unit: MU | 
| >300a |  00b4 | Source-Axis Distance: in mm | 
| >300a |  00b6 | Beam Limiting Device Sequence | 
| >>300a |  00b8 | RT Beam Limiting Device Type | 
| >>300a |  00ba | Source to Beam Limiting Device Dist | 
| >>300a |  00bc | Number of Leaf/Jaw Pairs | 
| >>300a |  00b8 | RT Beam Limiting Device Type | 
| >>300a |  00ba | Source to Beam Limiting Device Dist | 
| >>300a |  00bc | Number of Leaf/Jaw Pairs | 
| >>300a |  00be | Leaf Position Boundaries | 
| >300a |  00c0 | Beam Number | 
| >300a |  00c2 | Beam Name | 
| >300a |  00c3 | Beam Description | 
| >300a |  00c4 | Beam Type | 
| >300a |  00c6 | Radiation Type | 
| >300a |  00ce | Treatment Delivery Type | 
| >300a |  00d0 | Number of Wedges | 
| >300a |  00d1 | Wedge Sequence  | 
| >>300a |  00d2 | Wedge Number | 
| >>300a |  00d3 | Wedge Type | 
| >>300a |  00d4 | Wedge ID | 
| >>300a |  00d5 | Wedge Angle | 
| >>300a |  00d6 | Wedge Factor | 
| >>300a |  00d8 | Wedge Orientation | 
| >>300a |  00da | Source to Wedge Tray Distance | 
| >300a |  00e0 | Number of Compensators | 
| >300a |  00ed | Number of Boli | 
| >300a |  00f0 | Number of Blocks | 
| >300a |  00f2 | Total Block Tray Factor | 
| >300a |  00f4 | Block Sequence    | 
| >>300a |  00e1 | Material ID | 
| >>300a |  00f5 | Block Tray ID | 
| >>300a |  00f6 | Source to Block Tray Distance | 
| >>300a |  00f8 | Block Type | 
| >>300a |  00fa | Block Divergence | 
| >>300a |  00fb | Block Mounting Position | 
| >>300a |  00fc | Block Number | 
| >>300a |  00fe | Block Name | 
| >>300a |  0100 | Block Thickness | 
| >>300a |  0104 | Block Number of Points | 
| >>300a |  0106 | Block Data | 
| >300a |  0107 | Applicator Sequence **BRCheck**   | 
| >>300a |  0108 | Applicator ID | 
| >>300a |  0109 | Applicator Type | 
| >>300a |  010a | Applicator Description   **End BRCheck**| 
| >300c |  00b0 | Referenced Bolus Sequence    | 
| >>3006 |  0084 | Referenced ROI Number | 
| >>300a |  00dc | Bolus ID | 
| >>300a |  00dd | Bolus Description | 
| >300a |  010e | Final Cumulative Meterset Weight | 
| >300a |  0110 | Number of Control Points | 
| >300a |  0111 | Control Point Sequence  | 
| >>300a |  0112 | Control Point Index | 
| >>300a |  0114 | Nominal Beam Energy | 
| >>300a |  0116 | Wedge Position Sequence    | 
| >>>300a |  0118 | Wedge Position | 
| >>>300c |  00c0 | Referenced Wedge Number | 
| >>300a |  011a | Beam Limiting Device Position Sequence    | 
| >>>300a |  00b8 | RT Beam Limiting Device Type | 
| >>>300a |  011c | Leaf/Jaw Positions | 
| >>>300a |  00b8 | RT Beam Limiting Device Type | 
| >>>300a |  011c | Leaf/Jaw Positions | 
| >>300a |  011e | Gantry Angle | 
| >>300a |  011f | Gantry Rotation Direction | 
| >>300a |  0120 | Beam Limiting Device Angle | 
| >>300a |  0121 | Beam Limiting Device Rotation Direc | 
| >>300a |  0122 | Patient Support Angle | 
| >>300a |  0123 | Patient Support Rotation Direction | 
| >>300a |  0125 | Table Top Eccentric Angle | 
| >>300a |  0126 | Table Top Eccentric Rotation Direct | 
| >>300a |  0128 | Table Top Vertical Position | 
| >>300a |  0129 | Table Top Longitudinal Position | 
| >>300a |  012a | Table Top Lateral Position | 
| >>300a |  012c | Isocenter Position | 
| >>300a |  0130 | Source to Surface Distance | 
| >>300a |  0134 | Cumulative Meterset Weight | 
| >>300c |  0050 | Referenced Dose Reference Sequence    | 
| >>>300c |  0051 | Referenced Dose Reference Number | 

### C.8.8.16 Approval Module
| Group |  Element | Description |    
|-------|----------|-------------|
|  300e |  0002 | Approval Status | 
|  300e |  0004 | Review Date | 
|  300e |  0005 | Review Time | 
|  300e |  0008 | Reviewer Name | 

**BRCheck** (0018,0050) Slice Thickness Tag is exported by MIRS, but not required by CS.

**BRCheck** ('Tag not found: ', ['>300c', '006a', 'Referenced Patient Setup Number', 0])

**BRCheck** ('Tag not found: ', ['>300c', '00a0', 'Referenced Tolerance Table Number', 0])

**BRCheck** ('Tag not found: ', ['>300c', '006a', 'Referenced Patient Setup Number', 0])

**BRCheck** ('Tag not found: ', ['>300c', '00a0', 'Referenced Tolerance Table Number', 0])
