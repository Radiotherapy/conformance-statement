# Overview

## Purpose

This is the DICOM conformance statement for MIRS, the Modular Integrated Radiotherapy System for Radiotherapy and RadioSurgery. 

MIRS provides three-dimensional planning for several radiation treatment modalities such as Conformal Radiation Therapy (3D-CRT), Electron Beam Radiation Therapy (EBRT), Radio-surgery with Linear Accelerators and Intensity Modulated Radiation Therapy (IMRT). 

As of release 7.0.31, MIRS supports the network export of CT Images, RT Structure Sets, RT Plans, RT Dose and digitally reconstructed radiographs (DDRs) as RT Image objects. All this data is exported as DICOM objects to a  Provider of Service (SCP), or to files, selected by the user at export time.

A summary of the network services provided by MIRS can be found in the following tables:

**Table 1.1: Network Services**

| SOP Class             | User of Service 􏰀SCU􏰁 | Provider of Service 􏰀SCP􏰁 |
|-----------------------|-----------------------|---------------------------|
| **Transfer**          |                       |                           |
| CT Image Storage      | Yes                   | No                        |
| MR Image Storage      | Yes                   | No                        |
| STRUCTURE SET Storage | Yes                   | No                        |
| RT PLAN Storage       | Yes                   | No                        |
| RT Dose Storage       | Yes                   | No                        |
| RT Image Storage      | Yes                   | No                        |

**UID Values**

| UID Value                     | UID Name                 | Category |
|-------------------------------|--------------------------|----------|
| 1.2.840.10008.1.1             | Verification             | Transfer |
| 1.2.840.10008.5.1.4.1.1.2     | CT Image Storage         | Transfer |
| 1.2.840.10008.5.1.4.1.1.4     | MR Image Storage         | Transfer |
| 1.2.840.10008.5.1.4.1.1.481.3 | RT Structure Set Storage | Transfer |
| 1.2.840.10008.5.1.4.1.1.481.5 | RT Plan Storage          | Transfer |
| 1.2.840.10008.5.1.4.1.1.481.2 | RT Dose Storage          | Transfer |
| 1.2.840.10008.5.1.4.1.1.481.1 | RT Image Storage         | Transfer |

