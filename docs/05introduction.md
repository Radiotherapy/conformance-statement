# Introduction

## Revision History

**v 7.0.31**, *8/28/2018*: Initial draft release.

## Audience

This document is aimed for the users that need to understand the functionality of the DICOM export/import capabilities of MIRS. This document will be most useful for users that are responsible for setting up, managing and/or maintaining the DICOM network in their respective healthcare facilities. Any reader will be able to understand the implementation of the DICOM protocol by MIRS from this document. For further reference, the DICOM Standard is documented [here](http://dicom.nema.org/medical/dicom/current/output/html/part01.html).

## Remarks

The MIRS application is a three-dimensional treatment planning system for different radiation modalities. MIRS 
can import different images from files to its patient database. With the full visualization of the data, the user can design plans for 3D, IMRT, VMAT, 
(**NMED** *Aclarar los distintos tipos de planificaciones que MIRS brinda*), using state of the art dose calculation methods. 
Advanced optimization techniques are applied to enhance the quality of the treatments. These plans are can be exported to files, or stored in DICOM format to any DICOM compliant treatment management system.

## Document Conventions

(**NMED** *Al describir la funcionalidad, se puede aclarar que las opciones del usuario están en tal o cual solapa del menú. Hay que ver si vale la pena aclararlo o no*) 
Describe the menus and command calls if necessary. 

## Terms and definitions

Some of the terms and definitions from [DICOM Conformance Statement PS3.2](http://dicom.nema.org/medical/dicom/current/output/html/part02.html#sect_A.3.4)


**Abstract Syntax**:
The information agreed to be exchanged between applications, generally equivalent to a Service/Object Pair (SOP) Class. Examples: Verification SOP Class, Modality Worklist Information Model Find SOP Class, Computed Radiography Image Storage SOP Class.

**Application Entity (AE)**: An end point of a DICOM information exchange, including the DICOM network or media interface software; i.e., the software that sends or receives DICOM information objects or messages. A single device may have multiple Application Entities.

**Application Entity Title (AET)**: The externally known name of an Application Entity, used to identify a DICOM application to other DICOM applications on the network.

**Association**: A network communication channel set up between Application Entities.

**Attribute**:  A unit of information in an object definition; a data element identified by a tag. The information may be a complex data structure (Sequence), itself composed of lower level data elements. Examples: Patient ID (0010,0020), Accession Number (0008,0050), Photometric Interpretation (0028,0004), Procedure Code Sequence (0008,1032).

**Information Object Definition (IOD)**: The specified set of Attributes that comprise a type of data object; does not represent a specific instance of the data object, but rather a class of similar data objects that have the same properties. The Attributes may be specified as Mandatory (Type 1), Required but possibly unknown (Type 2), or Optional (Type 3), and there may be conditions associated with the use of an Attribute (Types 1C and 2C). Examples: MR Image IOD, CT Image IOD, Print Job IOD.

**Module**: A set of Attributes within an Information Object Definition that are logically related to each other. Example: Patient Module includes Patient Name, Patient ID, Patient Birth Date, and Patient Sex.

**Negotiation**: First phase of Association establishment that allows Application Entities to agree on the types of data to be exchanged and how that data will be encoded.

**Presentation Context**: The set of DICOM network services used over an Association, as negotiated between Application Entities; includes Abstract Syntaxes and Transfer Syntaxes.

**Protocol Data Unit (PDU)**: A packet (piece) of a DICOM message sent across the network. Devices must specify the maximum size packet they can receive for DICOM messages.

**Service Class Provider (SCP)**:
Role of an Application Entity that provides a DICOM network service; typically, a server that performs operations requested by another Application Entity (Service Class User). Examples: Picture Archiving and Communication System (image storage SCP, and image query/retrieve SCP), Radiology Information System (modality worklist SCP).

**Service Class User (SCU)**:
Role of an Application Entity that uses a DICOM network service; typically, a client. Examples: imaging modality (image storage SCU, and modality worklist SCU), imaging workstation (image query/retrieve SCU)

**Service/Object Pair Class (SOP Class)**:
The specification of the network or media transfer (service) of a particular type of data (object); the fundamental unit of DICOM interoperability specification. Examples: Ultrasound Image Storage Service, Basic Grayscale Print Management.

**Service/Object Pair Instance (SOP Instance)**:
An information object; a specific occurrence of information exchanged in a SOP Class. Examples: a specific x-ray image.

**Tag**: A 32-bit identifier for a data element, represented as a pair of four digit hexadecimal numbers, the “group” and the “element”. If the “group” number is odd, the tag is for a private (manufacturer-specific) data element. Examples: (0010,0020) [Patient ID], (07FE,0010) [Pixel Data], (0019,0210) [private data element]

**Transfer Syntax**:
The encoding used for exchange of DICOM information objects and messages. Examples: JPEG compressed (images), little endian explicit value representation.

**Unique Identifier (UID)**:
A globally unique “dotted decimal” string that identifies a specific object or a class of objects; an ISO-8824 Object Identifier. Examples: Study Instance UID, SOP Class UID, SOP Instance UID.

**Value Representation (VR)**:
The format type of an individual DICOM data element, such as text, an integer, a person’s name, or a code. DICOM information objects can be transmitted with either explicit identification of the type of each data element (Explicit VR), or without explicit identification (Implicit VR); with Implicit VR, the receiving application must use a DICOM data dictionary to look up the format of each data element.

## Basics of DICOM Communications

Two Application Entities (devices) that want to communicate with each other over a network using DICOM protocol must first agree on several things during an initial network "handshake". This communication is called Association, and is requested by one of those two devices. In the Association request, the requesting device ask for  specific services, information, and encoding  that are supported by the other device (Negotiation).

DICOM Standard defines network services and types of information objects, each of which is called an Abstract Syntax for the Negotiation. DICOM also specifies a variety of methods for encoding data, denoted Transfer Syntaxes. The Negotiation allows the initiating Application Entity to propose combinations of Abstract Syntax and Transfer Syntax to be used on the Association; these combinations are called Presentation Contexts. The receiving Application Entity accepts the Presentation Contexts it supports.

The Association Negotiation  allows the devices to agree on Roles - which one is the Service Class User (SCU - client) and which is the Service Class Provider (SCP - server). This agreement is determined for each Presentation Context. The Association Negotiation finally establishes  the exchange of maximum network packet (PDU) size, security information, and other network service options (called Extended Negotiation information).

Once the Application Entities had negotiated the Association parameters, they start to exchange data. Common data exchanges include queries for worklists and lists of stored images, transfer of image objects and analyses (structured reports), and sending images to film printers. Each exchangeable unit of data is formatted by the sender in accordance with the appropriate Information Object Definition, and sent using the negotiated Transfer Syntax. There is a Default Transfer Syntax that all systems must accept, but it may not be the most efficient for some use cases. Each transfer is explicitly acknowledged by the receiver with a Response Status indicating success, failure, or that query or retrieve operations are still in process.

Finally, the two Application Entities close the communication channel, when one of the request the release of the Association, and the other one acknowledges this request.

## Abbreviations

**3D**: 3 Dimension

**AE**: Application Entity

**CT**: Computed Tomography

**DICOM**: Digital Imaging and Communication

**DRR**: Digital Reconstructed Radiographs

**ID**: Identifier

**IMRT**: Intensity Modulated Radiation Therapy

**MLC**: Multileaf Collimator

**MR**: Magnetic Resonance

**NEMA**: National Electrical Manufacturers’ Association

**ROI**: Region of Interest

**RT**: Radiotherapy Treatment

**SCP**: Service Class Provider

**SCU**:Service Class User

**SOP**: Standard Operating Procedure

**UID**: Unique Identifier

**VM**: Value Multiplicity

**VMAT**: Volumetric Modulated Arc Therapy

**VR**: Value Representation

## References

 DICOM documents referred along the present guide are:

* [PS3.1: Introduction and Overview](http://dicom.nema.org/medical/dicom/current/output/html/part01.html)

* [PS3.2: Conformance](http://dicom.nema.org/medical/dicom/current/output/html/part02.html#PS3.2)

* [PS3.3: Information Object Definitions](http://dicom.nema.org/medical/dicom/current/output/html/part03.html#PS3.3)

* [PS3.4: Service Class Specifications](http://dicom.nema.org/medical/dicom/current/output/html/part04.html#PS3.4)

* [PS3.5: Data Structures and Encoding](http://dicom.nema.org/medical/dicom/current/output/html/part05.html#PS3.5)

* [PS3.7: Message Exchange](http://dicom.nema.org/medical/dicom/current/output/html/part07.html#PS3.7)

* [PS3.8: Network Communication Support for Message Exchange](http://dicom.nema.org/medical/dicom/current/output/html/part08.html#PS3.8)

Besides, the user can check [MIRS User's Manual]() for further guidance.

