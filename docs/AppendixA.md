
# Annexes

## Appendix A : Common Modules Export

### C.7.1 Patient Module Attributes     
| Group |  Element | Description | 
|-------|----------|-------------|
| 0010 |  0010 | Patient's Name |   
| 0010 |  0020 | Patient ID | 
| 0010 |  0030 | Patient's Birth Date | 
| 0010 |  0040 | Patient's Sex | 

### C.7.2.1. General Study Module Attributes
| Group |  Element | Description | 
|-------|----------|-------------|
| 0008 |  0020 | Study Date | 
| 0008 |  0030 | Study Time | 
| 0008 |  0050 | Accession Number | 
| 0008 |  0090 | Referring Physician's Name | 
| 0020 |  000d | Study Instance UID | 
| 0020 |  0010 | Study ID | 
| 0008 |  1030 | Study Description | 

### C.7.3.1 General Series Module
| Group |  Element | Description | 
|-------|----------|-------------|
| 0008 |  0060 | Modality | 
| 0008 |  1070 | Operators' Name | 
| 0018 |  5100 | Patient Position |    
| 0020 |  000e | Series Instance UID | 
| 0020 |  0011 | Series Number | 


### C.7.4.1 Frame of Reference Module
| Group |  Element | Description | 
|-------|----------|-------------|
| 0020 |  0052 | Frame of Reference UID |   
| 0020 |  1040 | Position Reference Indicator | 

### C.7.5.1 General Equipment Module
| Group |  Element | Description | 
|-------|----------|-------------|
| 0008 |  0070 | Manufacturer | 
| 0008 |  0080 | Institution Name | 
| 0008 |  1010 | Station Name | 
| 0008 |  1040 | Institutional Department Name | 
| 0008 |  1090 | Manufacturer's Model Name | 
| 0018 |  1000 | Device Serial Number | 
| 0018 |  1020 | Software Version | 

### C.7.6.1 General Image Module    
| Group |  Element | Description |    
|-------|----------|-------------|
| 0008 |  0023 | Content Date | 
| 0008 |  0033 | Content Time | 
| 0020 |  0012 | Acquisition Number | 
| 0020 |  0013 | Instance Number | 

### C.7.6.2 Image Plane Module (**BRCheck** CT only)  
| Group |  Element | Description |    
|-------|----------|-------------|
| 0018 |  0050 | Slice Thickness |    **BRCheck** present in RTPlan
| 0020 |  0032 | Image Position (Patie | 
| 0020 |  0037 | Image Orientation (Patie | 
| 0020 |  1041 | Slice Location | 
| 0028 |  0030 | Pixel Spacing | 

### C.7.6.3 Image Pixel Module
| Group |  Element | Description | 
|-------|----------|-------------|
| 0028 |  0010 | Rows | 
| 0028 |  0011 | Columns | 
| 0028 |  0100 | Bits Allocated | 
| 0028 |  0101 | Bits Stored | 
| 0028 |  0102 | High Bit | 
| 0028 |  0103 | Pixel Representation | 
| 7fe0 |  0010 | Pixel Data | 

### C.12.1 SOP Common Module
| Group |  Element | Description | 
|-------|----------|-------------|
| 0008 |  0005 | Specific Character Set | 
| 0008 |  0012 | Instance Creation Date | 
| 0008 |  0013 | Instance Creation Time | 
| 0008 |  0014 | Instance Creator UID |
| 0008 |  0016 | SOP Class UID | 
| 0008 |  0018 | SOP Instance UID | 


### C.11.2 VOI LUT Module
| Group |  Element | Description | 
|-------|----------|-------------|
| 0028 |  1050 | Window Center | 
| 0028 |  1051 | Window Width | 

### C.7.6.6. Multi-frame Module
| Group |  Element | Description | 
|-------|----------|-------------|
| 0028 |  0008 | Number of Frames | 
| 0028 |  0009 | Frame Increment Pointer | 

