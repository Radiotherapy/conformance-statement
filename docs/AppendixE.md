## Appendix E : RT Dose Export

C.7.1 Patient Module Attributes 

C.7.2.1. General Study Module Attributes 

C.7.3.1 General Series Module 

C.7.4.1 Frame of Reference Module 

C.7.5.1 General Equipment Module 

C.7.6.2 Image Plane Module (**BRCheck** CT only?) 

C.7.6.3 Image Pixel Module 

C.12.1 SOP Common Module 


### C.8.8.3 RT Dose Module
| Group |  Element | Description |    
|-------|----------|-------------|
| 0020 |  0013 | Instance Number | 
| 0028 |  0002 | Samples per Pixel | 
| 0028 |  0004 | Photometric Interpretation | 
| 0028 |  0100 | Bits Allocated | 
| 0028 |  0101 | Bits Stored | 
| 0028 |  0102 | High Bit | 
| 0028 |  0103 | Pixel Representation | 
| 3004 |  0002 | Dose Units | 
| 3004 |  0004 | Dose Type | 
| 3004 |  000a | Dose Summation Type | 
| 3004 |  000e | Dose Grid Scaling | 
| 3004 |  0014 | Tissue Heterogeneity Correction | 
| 300c |  0002 | Referenced RT Plan Sequence | 
| >0008 |  1150 | Referenced SOP Class UID | 
| >0008 |  1155 | Referenced SOP Instance UID | 
| 300c |  0060 | Referenced Structure Set Sequence  | 
| >0008 |  1150 | Referenced SOP Class UID | 
| >0008 |  1155 | Referenced SOP Instance UID | 
| 3004 |  000c | Grid Frame Offset Vector | 
| 7fe0 |  0010 | Pixel Data | 

