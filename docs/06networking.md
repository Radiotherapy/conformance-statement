# Networking

## Implementation Model

### Application Data Flow 

The interactions of MIRS with the DICOM world is illustrated in the following diagram:




### Functional Definition of MIRS SCU

MIRS users can configure different SCPs as targets for DICOM export. Each target is defined with the usual AE Title, IP address and port number. Then, the user selects the specific DICOM server from a customizable list of those target Application Entities, and chooses which DICOM objects (CT, MR, RT Plan, RT Dose, RT Structure and/or RT Image) will be stored in that target from a menu. The MIRS application requests the Association to the server, and sends the selected DICOM objects to that server once the Association is accepted. MIRS closes the Association once the transfer is completed.

#### CT Export

MIRS is able to export CT objects within a particular study. The user can choose to export all CT images  in native (512x512) or high resolution (1024x1024) pixels, select a signed Pixel Representation, and allow to export the images with rectangular pixels. Also, the user can select to export a specific set of slices from the CT image. By default, MIRS exports all CT slices in 512x512 pixels of square size, in unsigned pixel representation. 

#### MR Export

MIRS is able to export MR objects within a particular study. The user can choose to export all MR images  in native (512x512) or high resolution (1024x1024) pixels, select a signed Pixel Representation, and allow to export the images with rectangular pixels. Also, the user can select to export a specific set of slices from the MR image. By default, MIRS exports all MR slices in 512x512 pixels of square size, in unsigned pixel representation. 

#### RT Plan Export

MIRS exports the Plan as a DICOM RTPlan object, to the user-selected SCP Application Entity selected by the user as target. Course ID can be selected at export time.

#### RT Dose Export

MIRS exports the Dose as a DICOM RTDose object as a Full Anatomy Dose matrix, or selected planes over the principal axis. Oblique planes can not be exported.

#### RT Structure Export

MIRS exports the structures as DICOM RTStructure object. It can export any of the selected structures by the user.

#### RT Image Export

MIRS can export any of the DRR images as a DICOM RTImage object. The user can select the size, Window Width and Level. The Beam Collimation  overlay can be selected to be included in the exported DRR.

*a more detailed specification of each Application Entity, listing the SOP Classes supported and outlining the policies with which it initiates or accepts associations;*

*for each Application Entity and Real-World Activity combination, a description of proposed (for Association Initiation) and acceptable (for Association Acceptance) Presentation Contexts;*


## AE Specifications

### MIRS SCU

### SOP Classes

MIRS SCU Application Entity provides Standard Conformance to the following Storage SOP Classes.

| SOP Class Name            | SOP Class UID                 | SCU | SCP |
|---------------------------|-------------------------------|-----|-----|
| Verification              | 1.2.840.10008.1.1             | Yes | No  |
| CT Image Storage          | 1.2.840.10008.5.1.4.1.1.2     | Yes | No  |
| MR Image Storage          | 1.2.840.10008.5.1.4.1.1.4     | Yes | No  |
| RT Plan Storage           | 1.2.840.10008.5.1.4.1.1.481.5 | Yes | No  |
| RT Dose Storage           | 1.2.840.10008.5.1.4.1.1.481.2 | Yes | No  |
| RT Structure Set Storage  | 1.2.840.10008.5.1.4.1.1.481.3 | Yes | No  |
| RT Image Storage          | 1.2.840.10008.5.1.4.1.1.481.1 | Yes | No  |


MIRS provides an 

### Association Policies for MIRS SCU

#### General

The 􏰀PDU􏰁 size proposed in any association request is 4096 bytes, and it is not  configurable by the user.

#### Number of Associations
Table 3-4: Number of Associations as an Association Initiator for CMS_SCU

|Maximum number of simultaneous associations             | 1 | 
|------------------------------------------------------|---|

#### Asynchronous Nature
This is not supported.

####  Implementation Identifying Information

(**NMED**: *Completar la siguiente tabla, el UID es dependiente de la licencia? Habría que aclararlo*)
The implementation information for the Storage Application Entity is:

| Implementation Class UID	 | 1.3.6.1.4.1.32947 | 
|------------------------------------------------------|---|
| Implementation Version Name	 | MIRS_xxx| 



### Association Initiation Policy for MIRS SCU

The MIRS application requests the Association to the server, and sends the selected DICOM objects to that server once the Association is accepted. MIRS closes the Association once the transfer is completed. The default name for the MIRS AE title can be set by the user.

#### Description and Sequencing of activities

#### Proposed Presentation Contexts

| Presentation Context  |           Table                |                                 |                   |      |                      |
|----------------------------|---------------------------|---------------------------------|-------------------|------|----------------------|
| **Abstract Syntax**        |                               | **Transfer Syntax**             |            | **Role** | **Extended Negotiation** |
| **Name**                   | **UID**                       | **Name**                        | **UID**           |      |                      |
| CT Image Storage           | 1.2.840.10008.5.1.4.1.1.2     | DICOM Implicit VR Little Endian | 1.2.840.10008.1.2 | SCU  | None                 |
| MR Image Storage           | 1.2.840.10008.5.1.4.1.1.4     | DICOM Implicit VR Little Endian | 1.2.840.10008.1.2 | SCU  | None                 |
| RT Structure Set Storage   | 1.2.840.10008.5.1.4.1.1.481.3 | DICOM Implicit VR Little Endian | 1.2.840.10008.1.2 | SCU  | None                 |
| RT Dose Storage            | 1.2.840.10008.5.1.4.1.1.481.2 | DICOM Implicit VR Little Endian | 1.2.840.10008.1.2 | SCU  | None                 |
| RT Plan Storage            | 1.2.840.10008.5.1.4.1.1.481.5 | DICOM Implicit VR Little Endian | 1.2.840.10008.1.2 | SCU  | None                 |
| RT Image Storage           | 1.2.840.10008.5.1.4.1.1.481.1 | DICOM Implicit VR Little Endian | 1.2.840.10008.1.2 | SCU  | None                 |


#### SOP Specific Conformance for SOP Classes

DICOM Tag (0010,0010) for Patient Name and DICOM Tag (0010,0020) for Patient ID are the same for all the DICOM RT objects exported at a given time. DICOM Tag (0010,0030) for Patient's Birth Date and (0010,0040) are set at Patient creation within MIRS application, and inherited in each export.

Study instance UID (0020,000D) is persistent across different exports at different times. 

DICOM Tag (0008, 0070) for Manufacturer is **NUCLEMED SA** for all exported objects, as well as DICOM Tag (0008, 1090) Manufacturer's Model Name, which default to 'MIRS'.

##### SOP Specific Conformance for CT Image Storage SOP Class with MIRS SCU

For any given set of CT Slices, MIRS will export 256 slices, and it is not a "pass-through" transformation of the object.

##### SOP Specific Conformance for MR Image Storage SOP Class with MIRS SCU

For any given set of MR Slices, MIRS will export 256 slices, and it is not a "pass-through" transformation of the object.

##### SOP Specific Conformance for RT Plan Storage SOP Class with MIRS SCU

The export of each RT Plan generates a unique instance UID, therefore RT Plan object UIDs are not currently persistent.

##### SOP Specific Conformance for RT Dose Storage SOP Class with MIRS SCU

The export of a RT Dose object is always a fixed 128x128x128 array of dose values.
The export of each RT Dose generates a unique instance UID, therefore RT Dose object UIDs are not currently persistent.

##### SOP Specific Conformance for RT Structure Set Storage SOP Class with MIRS SCU

The export of each RT Structure Set generates a unique instance UID, therefore RT Structure object UIDs are not currently persistent.

##### SOP Specific Conformance for RT Image Storage SOP Class with MIRS SCU

DDR are exported as RT Image DICOM Object of 1024x1024 pixels, 
independently of the user-selected low, medium or high resolution used to compute each DRR, and irrespective to the Voltage chosen. The export of each RT Image generates a unique instance UID, therefore RT Image object UIDs are not currently persistent.

### MIRS SCP

At present time, MIRS does not provide SCP capabilities.


## Network Interfaces

### Physical Network Interface
The application is indifferent to the physical medium over which TCP/IP executes, which is dependent on the underlying operating system and hardware

### Additional Protocols
When host names rather than IP addresses are used in the configuration properties to specify presentation addresses for remote AEs, the application is dependent on the name resolution mechanism of the underlying operating system.

### IPv4 and IPv6 Support
This product supports both IPv4 and IPv6. It does not utilize any of the optional configuration identification or security features of IPv6.

It inherits the TCP/IP capabilities of the Windows operating system that it is running on.

## Configuration

MIRS SCU provides different configuration parameters, accessible to the end-user in the .... tab of the 
export menu.

## AE Title/Presentation Mapping

## Local AE Titles

Default local AE Title for MIRS SCU is 

## Remote AE Title/Presentation Address Mapping

Remote AE Title/Presentation Address Mapping can be configured by the end-user at ....

### Parameters

(**NMED** **BRCcheck** chequear qué cosas de las siguientes son configurables, y 
completar en consecuencia.)
|  General Parameters  | Configurable  (Yes/No)   |  Default  Value  |
|---------------------------------------------------------------------------------|---|---|
| Time-out waiting for acceptance or rejection Response to an Association Open Request. (Application Level timeout) |    |    |
| General DIMSE level time-out values |    |    |
| Time-out waiting for response to TCP/IP connect request. (Low-level timeout) |    |    |
| Time-out waiting for acceptance of a TCP/IP message over the network. (Low-level timeout) |    |    |
| Time-out for waiting for data between TCP/IP packets. (Low-level timeout) |    |    |
| Any changes to default TCP/IP settings, such as configurable stack parameters. |    |    |
| Definition of arbitrarily chosen origins |    |    |
| Definition of constant values used in Dose Related Distance Measurements |    |    |
| Other configurable parameters |    |    |
| **AE Specific Parameters** |    |    |
| Size constraint in maximum object size (see note) |    |    |
| Maximum PDU size the AE can send | No   |  4096 Bytes   |
| AE specific DIMSE level time-out values |    |    |
| Number of simultaneous Associations by Service and/or SOP Class |    |    |
| <SOP Class support> (e.g., Multi-frame vs. single frame vs. SC support), when configurable |    |    |
| <Transfer Syntax support>, e.g., JPEG, Explicit VR, when configurable |    |    |
| Other parameters that are configurable |    |    |
