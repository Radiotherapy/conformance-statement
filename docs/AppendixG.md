## Appendix G : RT Image Export

### C.7.6.1 General Image Module   
| Group |  Element | Description |    
|-------|----------|-------------|
| 0020 |  0013 | Instance Number | 
| 0020 |  0020 | Patient Orientation | 


### C.7.6.3 Image Pixel Module
| Group |  Element | Description | 
|-------|----------|-------------|
|  0028 |  0010 | Rows | 
|  0028 |  0011 | Columns | 
|  0028 |  0100 | Bits Allocated | 
|  0028 |  0101 | Bits Stored | 
|  0028 |  0102 | High Bit | 
|  0028 |  0103 | Pixel Representation | 
|  7fe0 |  0010 | Pixel Data | 

### C.8.6.1 SC Equipment Module
| Group |  Element | Description | 
|-------|----------|-------------|
| 0008 |  0060 | Modality | 


### C.8.8.2 RT Image Module
| Group |  Element | Description | 
|-------|----------|-------------|
| 0008 |  0008 | Image Type | 
| 0008 |  0064 | Conversion Type | 
| 0028 |  0002 | Samples per Pixel | 
| 0028 |  0004 | Photometric Interpretation | 
| 0028 |  0100 | Bits Allocated | 
| 0028 |  0101 | Bits Stored | 
| 0028 |  0102 | High Bit | 
| 0028 |  0103 | Pixel Representation | 
| 3002 |  0002 | RT Image Label | 
| 3002 |  000c | RT Image Plane | 
| 3002 |  000d | X-Ray Image Receptor Translation | 
| 3002 |  000e | X-Ray Image Receptor Angle | 
| 3002 |  0011 | Image Plane Pixel Spacing | 
| 3002 |  0012 | RT Image Position | 
| 3002 |  0020 | Radiation Machine Name | 
| 3002 |  0022 | Radiation Machine SAD | 
| 3002 |  0026 | RT Image SID | 
| 300a |  00b3 | Primary Dosimeter Unit | 
| 300a |  011e | Gantry Angle | 
| 300a |  0120 | Beam Limiting Device Angle | 
| 300a |  0122 | Patient Support Angle | 
| 300a |  0125 | Table Top Eccentric Angle | 
| 300c |  0002 | Referenced RT Plan Sequence | 
| >0008 |  1150 | Referenced SOP Class UID | 
| >0008 |  1155 | Referenced SOP Instance UID | 
| 300c |  0006 | Referenced Beam Number | 
| 300c |  0022 | Referenced Fraction Group Number | 
| 7fe0 |  0010 | Pixel Data | 

