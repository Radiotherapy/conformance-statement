## Appendix F : RT Structure Set Export

### C.8.8.5 Structure Set Module
| Group |  Element | Description |    
|-------|----------|-------------|
| 3006 |  0002 | Structure Set Label | 
| 3006 |  0004 | Structure Set Name | 
| 3006 |  0008 | Structure Set Date | 
| 3006 |  0009 | Structure Set Time | 
| 3006 |  0010 | Referenced Frame of Reference Sequence  | 
| >0020 |  0052 | Frame of Reference UID | 
| >3006 |  0012 | RT Referenced Study Sequence   1 item | 
| >>0008 |  1150 | Referenced SOP Class UID | 
| >>0008 |  1155 | Referenced SOP Instance UID | 
| >>3006 |  0014 | RT Referenced Series Sequence   1 item | 
| >>>0020 |  000e | Series Instance UID | 
| >>>3006 |  0016 | Contour Image Sequence   256 item | 
| >>>>0008 |  1150 | Referenced SOP Class UID | 
| >>>>0008 |  1155 | Referenced SOP Instance UID | 
| 3006 |  0020 | Structure Set ROI Sequence | 
| >3006 |  0022 | ROI Number | 
| >3006 |  0024 | Referenced Frame of Reference UID | 
| >3006 |  0026 | ROI Name | 
| >3006 |  002c | ROI Volume | 
| >3006 |  0036 | ROI Generation Algorithm | 

### C.8.8.6 ROI Contour Module
| Group |  Element | Description |    
|-------|----------|-------------|
|  3006 |  0039 | ROI Contour Sequence   3 item | 
| >3006 |  002a | ROI Display Color | 
| >3006 |  0040 | Contour Sequence   201 item | 
| >>3006 |  0016 | Contour Image Sequence   1 item | 
| >>>0008 |  1150 | Referenced SOP Class UID | 
| >>>0008 |  1155 | Referenced SOP Instance UID | 
| >>3006 |  0042 | Contour Geometric Type | 
| >>3006 |  0046 | Number of Contour Points | 
| >>3006 |  0050 | Contour Data | 
| >3006 |  0084 | Referenced ROI Number | 

### C.8.8.8 RT ROI Observations Module
| Group |  Element | Description |    
|-------|----------|-------------|
|  3006 |  0080 | RT ROI Observations Sequence |
| >3006 |  0082 | Observation Number | 
| >3006 |  0084 | Referenced ROI Number | 
| >3006 |  00a4 | RT ROI Interpreted Type | 
| >3006 |  00a6 | ROI Interpreter | 

