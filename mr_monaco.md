| 0008 |  0005 | Specific Character Set | 
| 0008 |  0008 | Image Type | 
| 0008 |  0012 | Instance Creation Date | 
| 0008 |  0013 | Instance Creation Time | 
| 0008 |  0016 | SOP Class UID | 
| 0008 |  0018 | SOP Instance UID | 
| 0008 |  0020 | Study Date | 
| 0008 |  0021 | Series Date | 
| 0008 |  0022 | Acquisition Date | 
| 0008 |  0023 | Content Date | 
| 0008 |  0030 | Study Time | 
| 0008 |  0031 | Series Time | 
| 0008 |  0032 | Acquisition Time | 
| 0008 |  0033 | Content Time | 
| 0008 |  0050 | Accession Number | 
| 0008 |  0060 | Modality | 
| 0008 |  0070 | Manufacturer | 
| 0008 |  0080 | Institution Name | 
| 0008 |  0081 | Institution Address | 
| 0008 |  0090 | Referring Physician's Name | 
| 0008 |  1010 | Station Name | 
| 0008 |  1030 | Study Description | 
| 0008 |  103e | Series Description | 
| 0008 |  1040 | Institutional Department Name | 
| 0008 |  1048 | Physician | 
| 0008 |  1050 | Performing Physician's Name | 
| 0008 |  1090 | Manufacturer's Model Name | 
| 0008 |  1110 | Referenced Study Sequence   1 item | 
| >0008 |  1150 | Referenced SOP Class UID | 
| >0008 |  1155 | Referenced SOP Instance UID | 
| 0008 |  1140 | Referenced Image Sequence   3 item | 
| >0008 |  1150 | Referenced SOP Class UID | 
| >0008 |  1155 | Referenced SOP Instance UID | 
| >0008 |  1150 | Referenced SOP Class UID | 
| >0008 |  1155 | Referenced SOP Instance UID | 
| >0008 |  1150 | Referenced SOP Class UID | 
| >0008 |  1155 | Referenced SOP Instance UID | 
| 0010 |  0010 | Patient's Name | 
| 0010 |  0020 | Patient ID | 
| 0010 |  0021 | Issuer of Patient ID | 
| 0010 |  0030 | Patient's Birth Date | 
| 0010 |  0032 | Patient's Birth Time | 
| 0010 |  0040 | Patient's Sex | 
| 0010 |  1010 | Patient's Age | 
| 0010 |  1030 | Patient's Weight | 
| 0010 |  1040 | Patient's Address | 
| 0018 |  0020 | Scanning Sequence | 
| 0018 |  0021 | Sequence Variant | 
| 0018 |  0022 | Scan Options | 
| 0018 |  0023 | MR Acquisition Type | 
| 0018 |  0024 | Sequence Name | 
| 0018 |  0025 | Angio Flag | 
| 0018 |  0050 | Slice Thickness | 
| 0018 |  0080 | Repetition Time | 
| 0018 |  0081 | Echo Time | 
| 0018 |  0082 | Inversion Time | 
| 0018 |  0083 | Number of Averages | 
| 0018 |  0084 | Imaging Frequency | 
| 0018 |  0085 | Imaged Nucleus | 
| 0018 |  0086 | Echo Number | 
| 0018 |  0087 | Magnetic Field Strength | 
| 0018 |  0089 | Number of Phase Encoding Steps | 
| 0018 |  0091 | Echo Train Length | 
| 0018 |  0093 | Percent Sampling | 
| 0018 |  0094 | Percent Phase Field of View | 
| 0018 |  0095 | Pixel Bandwidth | 
| 0018 |  1000 | Device Serial Number | 
| 0018 |  1020 | Software Version | 
| 0018 |  1030 | Protocol Name | 
| 0018 |  1251 | Transmit Coil Name | 
| 0018 |  1310 | Acquisition Matrix | 
| 0018 |  1312 | In-plane Phase Encoding Direction | 
| 0018 |  1314 | Flip Angle | 
| 0018 |  1315 | Variable Flip Angle Flag | 
| 0018 |  1316 | SAR | 
| 0018 |  1318 | dB/dt | 
| 0018 |  5100 | Patient Position | 
| 0019 |  1008 | [CSA Image Header Type] | 
| 0019 |  1009 | [CSA Image Header Version ??] | 
| 0019 |  100b | [SliceMeasurementDuration] | 
| 0019 |  100f | [GradientMode] | 
| 0019 |  1011 | [FlowCompensation] | 
| 0019 |  1012 | [TablePositionOrigin] | 
| 0019 |  1013 | [ImaAbsTablePosition] | 
| 0019 |  1014 | [ImaRelTablePosition] | 
| 0019 |  1015 | [SlicePosition_PCS] | 
| 0019 |  1017 | [SliceResolution] | 
| 0019 |  1018 | [RealDwellTime] | 
| 0020 |  000d | Study Instance UID | 
| 0020 |  000e | Series Instance UID | 
| 0020 |  0010 | Study ID | 
| 0020 |  0011 | Series Number | 
| 0020 |  0012 | Acquisition Number | 
| 0020 |  0013 | Instance Number | 
| 0020 |  0032 | Image Position (Patie | 
| 0020 |  0037 | Image Orientation (Patie | 
| 0020 |  0052 | Frame of Reference UID | 
| 0020 |  1040 | Position Reference Indicator | 
| 0020 |  1041 | Slice Location | 
| 0028 |  0002 | Samples per Pixel | 
| 0028 |  0004 | Photometric Interpretation | 
| 0028 |  0010 | Rows | 
| 0028 |  0011 | Columns | 
| 0028 |  0030 | Pixel Spacing | 
| 0028 |  0100 | Bits Allocated | 
| 0028 |  0101 | Bits Stored | 
| 0028 |  0102 | High Bit | 
| 0028 |  0103 | Pixel Representation | 
| 0028 |  0106 | Smallest Image Pixel Value | 
| 0028 |  0107 | Largest Image Pixel Value | 
| 0028 |  1050 | Window Center | 
| 0028 |  1051 | Window Width | 
| 0028 |  1055 | Window Center & Width Explanation | 
| 0029 |  1008 | [CSA Image Header Type] | 
| 0029 |  1009 | [CSA Image Header Version] | 
| 0029 |  1010 | [CSA Image Header Info] | 
| 0029 |  1018 | [CSA Series Header Type] | 
| 0029 |  1019 | [CSA Series Header Version] | 
| 0029 |  1020 | [CSA Series Header Info] | 
| 0029 |  1160 | [Series Workflow Status] | 
| 0032 |  000a | Study Status ID | 
| 0032 |  1032 | Requesting Physician | 
| 0032 |  1033 | Requesting Service | 
| 0032 |  1060 | Requested Procedure Description | 
| 0040 |  0244 | Performed Procedure Step Start Date | 
| 0040 |  0245 | Performed Procedure Step Start Time | 
| 0040 |  0253 | Performed Procedure Step ID | 
| 0040 |  0254 | Performed Procedure Step Descriptio | 
| 0040 |  0275 | Request Attributes Sequence   1 item | 
| >0040 |  0007 | Scheduled Procedure Step Descriptio | 
| >0040 |  0009 | Scheduled Procedure Step ID | 
| >0040 |  1001 | Requested Procedure ID | 
| 0051 |  1008 | [CSA Image Header Type] | 
| 0051 |  1009 | [CSA Image Header Version ??] | 
| 0051 |  100a | [Unknown] | 
| 0051 |  100b | [AcquisitionMatrixText] | 
| 0051 |  100c | [Unknown] | 
| 0051 |  100d | [Unknown] | 
| 0051 |  100e | [Unknown] | 
| 0051 |  100f | [CoilString] | 
| 0051 |  1011 | [PATModeText] | 
| 0051 |  1012 | [Unknown] | 
| 0051 |  1013 | [PositivePCSDirections] | 
| 0051 |  1015 | [Unknown] | 
| 0051 |  1016 | [Unknown] | 
| 0051 |  1017 | [Unknown] | 
| 0051 |  1019 | [Unknown] | 
| 7fe0 |  0010 | Pixel Data | 
