

| 0018 |  0050 | Slice Thickness | 

| 300a |  0002 | RT Plan Label | 
| 300a |  0003 | RT Plan Name | 
| 300a |  0004 | RT Plan Description | 
| 300a |  0006 | RT Plan Date | 
| 300a |  0007 | RT Plan Time | 
| 300a |  000c | RT Plan Geometry | 
| 300a |  0010 | Dose Reference Sequence   1 item | 
| >300a |  0012 | Dose Reference Number | 
| >300a |  0014 | Dose Reference Structure Type | 
| >300a |  0016 | Dose Reference Description | 
| >300a |  0020 | Dose Reference Type | 
| >300a |  0026 | Target Prescription Dose | 
| 300a |  0040 | Tolerance Table Sequence   1 item | 
| >300a |  0042 | Tolerance Table Number | 
| >300a |  0043 | Tolerance Table Label | 
| 300a |  0070 | Fraction Group Sequence   1 item | 
| >300a |  0071 | Fraction Group Number | 
| >300a |  0078 | Number of Fractions Planned | 
| >300a |  0080 | Number of Beams | 
| >300a |  00a0 | Number of Brachy Application Setups | 
| >300c |  0004 | Referenced Beam Sequence   2 item | 
| >>300a |  0082 | Beam Dose Specification Point | 
| >>300a |  0084 | Beam Dose | 
| >>300a |  0086 | Beam Meterset | 
| >>300c |  0006 | Referenced Beam Number | 
| >>300a |  0082 | Beam Dose Specification Point | 
| >>300a |  0084 | Beam Dose | 
| >>300a |  0086 | Beam Meterset | 
| >>300c |  0006 | Referenced Beam Number | 
| >300c |  0050 | Referenced Dose Reference Sequence   1 item | 
| >>300c |  0051 | Referenced Dose Reference Number | 
| 300a |  00b0 | Beam Sequence   2 item | 
| >300a |  00b2 | Treatment Machine Name | 
| >300a |  00b3 | Primary Dosimeter Unit | 
| >300a |  00b4 | Source-Axis Distance | 
| >300a |  00b6 | Beam Limiting Device Sequence   2 item | 
| >>300a |  00b8 | RT Beam Limiting Device Type | 
| >>300a |  00ba | Source to Beam Limiting Device Dist | 
| >>300a |  00bc | Number of Leaf/Jaw Pairs | 
| >>300a |  00b8 | RT Beam Limiting Device Type | 
| >>300a |  00ba | Source to Beam Limiting Device Dist | 
| >>300a |  00bc | Number of Leaf/Jaw Pairs | 
| >>300a |  00be | Leaf Position Boundaries | 
| >300a |  00c0 | Beam Number | 
| >300a |  00c2 | Beam Name | 
| >300a |  00c3 | Beam Description | 
| >300a |  00c4 | Beam Type | 
| >300a |  00c6 | Radiation Type | 
| >300a |  00ce | Treatment Delivery Type | 
| >300a |  00d0 | Number of Wedges | 
| >300a |  00d1 | Wedge Sequence   1 item | 
| >>300a |  00d2 | Wedge Number | 
| >>300a |  00d3 | Wedge Type | 
| >>300a |  00d4 | Wedge ID | 
| >>300a |  00d5 | Wedge Angle | 
| >>300a |  00d6 | Wedge Factor | 
| >>300a |  00d8 | Wedge Orientation | 
| >>300a |  00da | Source to Wedge Tray Distance | 
| >300a |  00e0 | Number of Compensators | 
| >300a |  00ed | Number of Boli | 
| >300a |  00f0 | Number of Blocks | 
| >300a |  010e | Final Cumulative Meterset Weight | 
| >300a |  0110 | Number of Control Points | 
| >300a |  0111 | Control Point Sequence   4 item | 
| >>300a |  0112 | Control Point Index | 
| >>300a |  0114 | Nominal Beam Energy | 
| >>300a |  0116 | Wedge Position Sequence   1 item | 
| >>>300a |  0118 | Wedge Position | 
| >>>300c |  00c0 | Referenced Wedge Number | 
| >>300a |  011a | Beam Limiting Device Position Sequence   2 item | 
| >>>300a |  00b8 | RT Beam Limiting Device Type | 
| >>>300a |  011c | Leaf/Jaw Positions | 
| >>>300a |  00b8 | RT Beam Limiting Device Type | 
| >>>300a |  011c | Leaf/Jaw Positions | 
| >>300a |  011e | Gantry Angle | 
| >>300a |  011f | Gantry Rotation Direction | 
| >>300a |  0120 | Beam Limiting Device Angle | 
| >>300a |  0121 | Beam Limiting Device Rotation Direc | 
| >>300a |  0122 | Patient Support Angle | 
| >>300a |  0123 | Patient Support Rotation Direction | 
| >>300a |  0125 | Table Top Eccentric Angle | 
| >>300a |  0126 | Table Top Eccentric Rotation Direct | 
| >>300a |  0128 | Table Top Vertical Position | 
| >>300a |  0129 | Table Top Longitudinal Position | 
| >>300a |  012a | Table Top Lateral Position | 
| >>300a |  012c | Isocenter Position | 
| >>300a |  0130 | Source to Surface Distance | 
| >>300a |  0134 | Cumulative Meterset Weight | 
| >>300c |  0050 | Referenced Dose Reference Sequence   1 item | 
| >>>300c |  0051 | Referenced Dose Reference Number | 
| >>300a |  0112 | Control Point Index | 
| >>300a |  0114 | Nominal Beam Energy | 
| >>300a |  0116 | Wedge Position Sequence   1 item | 
| >>>300a |  0118 | Wedge Position | 
| >>>300c |  00c0 | Referenced Wedge Number | 
| >>300a |  011e | Gantry Angle | 
| >>300a |  011f | Gantry Rotation Direction | 
| >>300a |  0134 | Cumulative Meterset Weight | 
| >>300c |  0050 | Referenced Dose Reference Sequence   1 item | 
| >>>300c |  0051 | Referenced Dose Reference Number | 
| >>300a |  0112 | Control Point Index | 
| >>300a |  0114 | Nominal Beam Energy | 
| >>300a |  0116 | Wedge Position Sequence   1 item | 
| >>>300a |  0118 | Wedge Position | 
| >>>300c |  00c0 | Referenced Wedge Number | 
| >>300a |  0134 | Cumulative Meterset Weight | 
| >>300c |  0050 | Referenced Dose Reference Sequence   1 item | 
| >>>300c |  0051 | Referenced Dose Reference Number | 
| >>300a |  0112 | Control Point Index | 
| >>300a |  0114 | Nominal Beam Energy | 
| >>300a |  0116 | Wedge Position Sequence   1 item | 
| >>>300a |  0118 | Wedge Position | 
| >>>300c |  00c0 | Referenced Wedge Number | 
| >>300a |  0134 | Cumulative Meterset Weight | 
| >>300c |  0050 | Referenced Dose Reference Sequence   1 item | 
| >>>300c |  0051 | Referenced Dose Reference Number | 
| >300c |  006a | Referenced Patient Setup Number | 
| >300c |  00a0 | Referenced Tolerance Table Number | 
| >300a |  00b2 | Treatment Machine Name | 
| >300a |  00b3 | Primary Dosimeter Unit | 
| >300a |  00b4 | Source-Axis Distance | 
| >300a |  00b6 | Beam Limiting Device Sequence   2 item | 
| >>300a |  00b8 | RT Beam Limiting Device Type | 
| >>300a |  00ba | Source to Beam Limiting Device Dist | 
| >>300a |  00bc | Number of Leaf/Jaw Pairs | 
| >>300a |  00b8 | RT Beam Limiting Device Type | 
| >>300a |  00ba | Source to Beam Limiting Device Dist | 
| >>300a |  00bc | Number of Leaf/Jaw Pairs | 
| >>300a |  00be | Leaf Position Boundaries | 
| >300a |  00c0 | Beam Number | 
| >300a |  00c2 | Beam Name | 
| >300a |  00c3 | Beam Description | 
| >300a |  00c4 | Beam Type | 
| >300a |  00c6 | Radiation Type | 
| >300a |  00ce | Treatment Delivery Type | 
| >300a |  00d0 | Number of Wedges | 
| >300a |  00d1 | Wedge Sequence   1 item | 
| >>300a |  00d2 | Wedge Number | 
| >>300a |  00d3 | Wedge Type | 
| >>300a |  00d4 | Wedge ID | 
| >>300a |  00d5 | Wedge Angle | 
| >>300a |  00d6 | Wedge Factor | 
| >>300a |  00d8 | Wedge Orientation | 
| >>300a |  00da | Source to Wedge Tray Distance | 
| >300a |  00e0 | Number of Compensators | 
| >300a |  00ed | Number of Boli | 
| >300a |  00f0 | Number of Blocks | 
| >300a |  010e | Final Cumulative Meterset Weight | 
| >300a |  0110 | Number of Control Points | 
| >300a |  0111 | Control Point Sequence   4 item | 
| >>300a |  0112 | Control Point Index | 
| >>300a |  0114 | Nominal Beam Energy | 
| >>300a |  0116 | Wedge Position Sequence   1 item | 
| >>>300a |  0118 | Wedge Position | 
| >>>300c |  00c0 | Referenced Wedge Number | 
| >>300a |  011a | Beam Limiting Device Position Sequence   2 item | 
| >>>300a |  00b8 | RT Beam Limiting Device Type | 
| >>>300a |  011c | Leaf/Jaw Positions | 
| >>>300a |  00b8 | RT Beam Limiting Device Type | 
| >>>300a |  011c | Leaf/Jaw Positions | 
| >>300a |  011e | Gantry Angle | 
| >>300a |  011f | Gantry Rotation Direction | 
| >>300a |  0120 | Beam Limiting Device Angle | 
| >>300a |  0121 | Beam Limiting Device Rotation Direc | 
| >>300a |  0122 | Patient Support Angle | 
| >>300a |  0123 | Patient Support Rotation Direction | 
| >>300a |  0125 | Table Top Eccentric Angle | 
| >>300a |  0126 | Table Top Eccentric Rotation Direct | 
| >>300a |  0128 | Table Top Vertical Position | 
| >>300a |  0129 | Table Top Longitudinal Position | 
| >>300a |  012a | Table Top Lateral Position | 
| >>300a |  012c | Isocenter Position | 
| >>300a |  0130 | Source to Surface Distance | 
| >>300a |  0134 | Cumulative Meterset Weight | 
| >>300c |  0050 | Referenced Dose Reference Sequence   1 item | 
| >>>300c |  0051 | Referenced Dose Reference Number | 
| >>300a |  0112 | Control Point Index | 
| >>300a |  0114 | Nominal Beam Energy | 
| >>300a |  0116 | Wedge Position Sequence   1 item | 
| >>>300a |  0118 | Wedge Position | 
| >>>300c |  00c0 | Referenced Wedge Number | 
| >>300a |  011e | Gantry Angle | 
| >>300a |  011f | Gantry Rotation Direction | 
| >>300a |  0134 | Cumulative Meterset Weight | 
| >>300c |  0050 | Referenced Dose Reference Sequence   1 item | 
| >>>300c |  0051 | Referenced Dose Reference Number | 
| >>300a |  0112 | Control Point Index | 
| >>300a |  0114 | Nominal Beam Energy | 
| >>300a |  0116 | Wedge Position Sequence   1 item | 
| >>>300a |  0118 | Wedge Position | 
| >>>300c |  00c0 | Referenced Wedge Number | 
| >>300a |  0134 | Cumulative Meterset Weight | 
| >>300c |  0050 | Referenced Dose Reference Sequence   1 item | 
| >>>300c |  0051 | Referenced Dose Reference Number | 
| >>300a |  0112 | Control Point Index | 
| >>300a |  0114 | Nominal Beam Energy | 
| >>300a |  0116 | Wedge Position Sequence   1 item | 
| >>>300a |  0118 | Wedge Position | 
| >>>300c |  00c0 | Referenced Wedge Number | 
| >>300a |  0134 | Cumulative Meterset Weight | 
| >>300c |  0050 | Referenced Dose Reference Sequence   1 item | 
| >>>300c |  0051 | Referenced Dose Reference Number | 
| >300c |  006a | Referenced Patient Setup Number | 
| >300c |  00a0 | Referenced Tolerance Table Number | 
| 300a |  0180 | Patient Setup Sequence   1 item |  
| >0018 |  5100 | Patient Position | 
| >300a |  0182 | Patient Setup Number | 
| 300c |  0060 | Referenced Structure Set Sequence   1 item | 
| >0008 |  1150 | Referenced SOP Class UID | 
| >0008 |  1155 | Referenced SOP Instance UID | 
| 300e |  0002 | Approval Status | 
| 300e |  0004 | Review Date | 
| 300e |  0005 | Review Time | 
| 300e |  0008 | Reviewer Name | 
