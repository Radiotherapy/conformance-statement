Reading:  ../../VALIDACION_Studies/Test MR coronal/v7.0.14/IMAGE_STUDY_mr coronal_Slice115_PatID_INV0050_CaseID_001_20181113211634.DCM
(0008, 0008) Image Type                          CS: ['DERIVED', 'SECONDARY', 'CORONAL']
(0008, 0012) Instance Creation Date              DA: '20181113'
(0008, 0013) Instance Creation Time              TM: '211634'
(0008, 0014) Instance Creator UID                UI: 1.3.6.1.4.1.32947
(0008, 0016) SOP Class UID                       UI: MR Image Storage
(0008, 0018) SOP Instance UID                    UI: 1.3.6.1.4.1.32947.4.20181113205659.7.0.115
(0008, 0020) Study Date                          DA: '20181113'
(0008, 0023) Content Date                        DA: '20181113'
(0008, 0030) Study Time                          TM: '211634'
(0008, 0033) Content Time                        TM: '211634'
(0008, 0050) Accession Number                    SH: '1'
(0008, 0060) Modality                            CS: 'MR'
(0008, 0070) Manufacturer                        LO: 'NUCLEMED SA'
(0008, 0080) Institution Name                    LO: 'NUCLEMED'
(0008, 0090) Referring Physician's Name          PN: 'capp'
(0008, 1010) Station Name                        SH: 'MIRS_214B-16339'
(0008, 1030) Study Description                   LO: 'Processed by MIRS'
(0008, 1040) Institutional Department Name       LO: 'Software Department'
(0008, 1070) Operators' Name                     PN: 'Administrator'
(0008, 1090) Manufacturer's Model Name           LO: 'MIRS'
(0010, 0010) Patient's Name                      PN: 'RESEARCH50^RESEARCH50'
(0010, 0020) Patient ID                          LO: 'INV0050'
(0010, 0030) Patient's Birth Date                DA: '19500101'
(0010, 0040) Patient's Sex                       CS: 'M'
(0018, 0050) Slice Thickness                     DS: "0.5"
(0018, 1000) Device Serial Number                LO: '214B-16339'
(0018, 1020) Software Version(s)                 LO: '7.0'
(0018, 1100) Reconstruction Diameter             DS: ''
(0018, 1111) Distance Source to Patient          DS: ''
(0018, 1120) Gantry/Detector Tilt                DS: "0"
(0018, 1130) Table Height                        DS: ''
(0018, 5100) Patient Position                    CS: 'HFS'
(0020, 000d) Study Instance UID                  UI: 1.3.6.1.4.1.32947.20181113205832
(0020, 000e) Series Instance UID                 UI: 1.3.6.1.4.1.32947.20181113205832.0
(0020, 0010) Study ID                            SH: '1'
(0020, 0011) Series Number                       IS: '1'
(0020, 0012) Acquisition Number                  IS: '1'
(0020, 0013) Instance Number                     IS: '115'
(0020, 0032) Image Position (Patient)            DS: ['-134.775400', '-75.448240', '134.775400']
(0020, 0037) Image Orientation (Patient)         DS: ['1.00', '0.00', '0.00', '0.00', '0.00', '-1.00']
(0020, 0052) Frame of Reference UID              UI: 1.3.6.1.4.1.32947.20181113205832.0.0
(0020, 1040) Position Reference Indicator        LO: ''
(0020, 1041) Slice Location                      DS: "-75.448"
(0028, 0002) Samples per Pixel                   US: 1
(0028, 0004) Photometric Interpretation          CS: 'MONOCHROME2'
(0028, 0010) Rows                                US: 512
(0028, 0011) Columns                             US: 512
(0028, 0030) Pixel Spacing                       DS: ['0.527', '0.527']
(0028, 0100) Bits Allocated                      US: 16
(0028, 0101) Bits Stored                         US: 16
(0028, 0102) High Bit                            US: 15
(0028, 0103) Pixel Representation                US: 1
(0028, 1050) Window Center                       DS: "191"
(0028, 1051) Window Width                        DS: "382"
(0028, 1052) Rescale Intercept                   DS: "0"
(0028, 1053) Rescale Slope                       DS: "1"
(7fe0, 0010) Pixel Data                          OW: Array of 524288 bytes
