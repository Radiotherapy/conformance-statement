| 0008 |  0008 | Image Type | 
| 0008 |  0012 | Instance Creation Date | 
| 0008 |  0013 | Instance Creation Time | 
| 0008 |  0014 | Instance Creator UID | 
| 0008 |  0016 | SOP Class UID | 
| 0008 |  0018 | SOP Instance UID | 
| 0008 |  0020 | Study Date | 
| 0008 |  0030 | Study Time | 
| 0008 |  0050 | Accession Number | 
| 0008 |  0060 | Modality | 
| 0008 |  0064 | Conversion Type | 
| 0008 |  0070 | Manufacturer | 
| 0008 |  0080 | Institution Name | 
| 0008 |  0090 | Referring Physician's Name | 
| 0008 |  1010 | Station Name | 
| 0008 |  1040 | Institutional Department Name | 
| 0008 |  1070 | Operators' Name | 
| 0008 |  1090 | Manufacturer's Model Name | 
| 0010 |  0010 | Patient's Name | 
| 0010 |  0020 | Patient ID | 
| 0010 |  0030 | Patient's Birth Date | 
| 0010 |  0040 | Patient's Sex | 
| 0020 |  000d | Study Instance UID | 
| 0020 |  000e | Series Instance UID | 
| 0020 |  0010 | Study ID | 
| 0020 |  0011 | Series Number | 
| 0020 |  0013 | Instance Number | 
| 0020 |  0020 | Patient Orientation | 
| 0020 |  0052 | Frame of Reference UID | 

| 0028 |  0002 | Samples per Pixel | 
| 0028 |  0004 | Photometric Interpretation | 
| 0028 |  0010 | Rows | 
| 0028 |  0011 | Columns | 
| 0028 |  0100 | Bits Allocated | 
| 0028 |  0101 | Bits Stored | 
| 0028 |  0102 | High Bit | 
| 0028 |  0103 | Pixel Representation | 
| 0028 |  1050 | Window Center | 
| 0028 |  1051 | Window Width | 

